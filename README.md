# -*- coding: utf-8 -*-
"""
Created on Sat Jun 13 19:26:24 2020

@author: Michalina Szymańska, Sebastian Łakomiec"""

# ładowanie niezbędnych pakietów
import numpy as np
import pandas as pd

death = pd.read_csv("dane/PoliceKillingsUS.csv", encoding='mac_roman')
ile = death["threat_level"].value_counts()
print(ile)
ile.iloc[np.argsort(ile.index)]

death.info()

# zmiana typów danych z 'object' główne na string, (oraz int)
death.id = death.id.astype("int")
death.name = death.name.astype("string")
death.manner_of_death = death.manner_of_death.astype("string")
death.armed = death.armed.astype("string")

# ValueError: Cannot convert non-finite values (NA or inf) to integer
#death.age = death.age.astype("int")

death.gender = death.gender.astype("string")
death.race = death.race.astype("string")
death.city = death.city.astype("string")
death.state = death.state.astype("string")
death.threat_level = death.threat_level.astype("string")
death.flee = death.flee.astype("string")

death.info()








